//  Copyright © 2020 Sampath Dassanayake. All rights reserved.

import SwiftUI

struct TypeCell: View {
    var type: Type
    
    var body: some View {
        HStack(alignment: .center, spacing: 15.0, content: {
            Text(self.type.displayName)
                .font(.custom("American Typewriter", size: 48.0))
                .foregroundColor(Color("Home"))
            Divider()
            Text(self.type.description)
                .font(.custom("American Typewriter", size: 16.0))
                .lineLimit(2)
                .foregroundColor(Color("Home"))
        })
        .padding(.init(top: 10.0, leading: 15.0, bottom: 10.0, trailing: 10.0))
        .frame(minWidth: 0, maxWidth: .infinity, minHeight: 0, maxHeight: .infinity, alignment: .leading)
        .overlay(
            RoundedRectangle(cornerRadius: 15.0)
                .stroke(lineWidth: 2.0)
                .foregroundColor(.gray)
        )
    }
}

struct TypeCell_Previews: PreviewProvider {
    static var previews: some View {
        Group {
            TypeCell(type: Type.init(stringLiteral: "alpha"))
            TypeCell(type: Type.init(stringLiteral: "beta"))
        }
        .previewLayout(.fixed(width: 375, height: 120))
    }
}
