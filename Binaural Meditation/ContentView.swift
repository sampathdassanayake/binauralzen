//  Copyright © 2020 Sampath Dassanayake. All rights reserved.

import SwiftUI

struct ContentView: View {
    @State private var selection: Tabs = .home
    
    private enum Tabs: Hashable {
        case home
        case history
        case about
    }
 
    var body: some View {
        TabView(selection: $selection) {
            TypesListSwiftUIView()
                .tabItem {
                    VStack(alignment: .center) {
                        if selection == Tabs.home {
                            Image(systemName: "music.house.fill")
                        } else {
                            Image(systemName: "music.house")
                        }
                    }
            }.tag(Tabs.home)
            HistoryView()
                .tabItem {
                    VStack {
                        if selection == Tabs.history {
                            Image(systemName: "archivebox.fill")
                        } else {
                            Image(systemName: "archivebox")
                        }
                    }
            }.tag(Tabs.history)
            InfoView()
            .tabItem {
                VStack {
                    if selection == Tabs.about {
                        Image(systemName: "info.circle.fill")
                    } else {
                        Image(systemName: "info.circle")
                    }
                }
            }.tag(Tabs.about)
            
        }.accentColor(Color("Home"))
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
