//  Copyright © 2020 Sampath Dassanayake. All rights reserved.


import Foundation
import UIKit

typealias FrequencyRange = (Int, Int)
enum SoundType  {
    case binaural
    case noise
}

struct Type: ExpressibleByStringLiteral, Hashable, Equatable, Identifiable {
    var id: String { rawValue }
    var type : SoundType
    var rawValue: String
    var displayName: String
    var description: String
    var backgroundImage: String?
    var backgroundColor: UIColor
    var frequencyRange: ClosedRange<Double>
    
    init(stringLiteral value: String){
        self.rawValue = value
        
        switch rawValue {
        case "delta":
            displayName = "δ"
            description = "Deep Sleep • Pain relief • Anti-aging • Healing"
            backgroundImage = nil
            backgroundColor = UIColor(named: "DeltaColor")!
            frequencyRange = 0.1...4
            type = .binaural
        case "theta":
            displayName = "θ"
            description = "REM Sleep • Deep relaxation • Meditation • Creativity"
            backgroundImage = nil
            backgroundColor = UIColor(named: "ThetaColor")!
            frequencyRange = 4...7
            type = .binaural
        case "alpha":
            displayName = "α"
            description = "Relaxed focus • Stress reduction • Positive thinking • Fast learning"
            backgroundImage = nil
            backgroundColor = UIColor(named: "AlphaColor")!
            frequencyRange = 8...13
            type = .binaural
        case "beta":
            displayName = "β"
            description = "Focused Attention • Cognitive thinking • Problem solving "
            backgroundImage = nil
            backgroundColor = UIColor(named: "BetaColor")!
            frequencyRange = 13...30
            type = .binaural
        case "gamma":
            displayName = "γ"
            description = "High-level cognition • Memory recall • Peak awareness"
            backgroundImage = nil
            backgroundColor = UIColor(named: "GammaColor")!
            frequencyRange = 30...40
            type = .binaural
        default:
            fatalError()
        }
        
    }
    
    static func == (lhs: Type, rhs: Type) -> Bool {
        return
            lhs.rawValue == rhs.rawValue
    }
    
    
}

enum Types: Type, CaseIterable, Identifiable {
    
    var id: String { rawValue.rawValue }
    
    case delta
    case theta
    case alpha
    case beta
    case gamma
}
