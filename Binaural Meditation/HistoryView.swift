//  Copyright © 2020 Sampath Dassanayake. All rights reserved.

import HealthKit
import HealthKitUI
import SwiftUI

struct HistoryView: View {
    let healthStore = HKHealthStore()
    @State var results: [HKSample] = []
    
    var body: some View {
        VStack {
            Text("History").font(.custom("American Typewriter", size: 18.0)).bold()
            List(results, id: \.self) { sample in
                HistoryRow(sample: sample)
            }
            Text("History is fetched from your devices HealthKit if you have granted permission").font(.custom("American Typewriter", size: 12.0)).padding(.bottom, 5)
        }.onAppear() {
            Helpers.activateHealthKit { (result, error) in
                if result {
                    self.retrieveMindFulMinutes()
                }
            }
        }
    }
    
    func retrieveMindFulMinutes() {

        if let categoryType = HKObjectType.categoryType(forIdentifier: .mindfulSession) {
            // Use a sortDescriptor to get the recent data first (optional)
            let sortDescriptor = NSSortDescriptor(key: HKSampleSortIdentifierEndDate, ascending: false)

            // Get all samples from the last 24 hours
            let endDate = Date()
            let startDate = endDate.addingTimeInterval(-30.0 * 60.0 * 60.0 * 24.0)
            let predicate = HKQuery.predicateForSamples(withStart: startDate, end: endDate, options: [])

            // Create the HealthKit Query
            let query = HKSampleQuery(
                sampleType: categoryType,
                predicate: predicate,
                limit: 0,
                sortDescriptors: [sortDescriptor],
                resultsHandler: updateHistory
            )
            // Execute our query
            healthStore.execute(query)
        }
    }
    
    func updateHistory(query: HKSampleQuery, results: [HKSample]?, error: Error?) {
        self.results = results ?? []
    }
}

struct HistoryView_Previews: PreviewProvider {
    static var previews: some View {
        HistoryView()
    }
}

struct HistoryRow: View {
    var sample: HKSample
    
    var body: some View {
        VStack(alignment: .leading, spacing: 5.0) {
            
            Text(Helpers.dateFormatter().string(from: sample.startDate) + " for " + Helpers.secondsToTimeDisplayable(seconds: Int(sample.endDate.timeIntervalSince(sample.startDate))) + "Mins")
                .font(.custom("American Typewriter", size: 16.0))
                .foregroundColor(Color("Home"))
                Spacer()
        }
        .padding(.all, 10.0)
        .frame(minWidth: 0, maxWidth: .infinity, minHeight: 0, maxHeight: .infinity, alignment: .topLeading)
        .overlay(
            RoundedRectangle(cornerRadius: 15.0)
                .stroke(lineWidth: 2.0)
                .foregroundColor(.gray)
        )
//        .background(Color(self.type.backgroundColor))
//        .border(Color.gray, width: 1)
//        .cornerRadius(10.0)
    }
}
