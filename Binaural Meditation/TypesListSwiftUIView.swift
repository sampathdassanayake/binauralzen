//  Copyright © 2020 Sampath Dassanayake. All rights reserved.

import ModalView
import SwiftUI

struct TypesListSwiftUIView: View {
    @State var showWarning = true
    
    init() {
        UITableView.appearance().separatorStyle = .none
    }
    
    var body: some View {
        
        VStack {
            Spacer().frame(minHeight: 10, maxHeight: 20)
            VStack {
                HStack {
                    Text("Binaural Zen").font(.custom("American Typewriter", size: 20.0))
                    Spacer()
                }
                HStack {
                    Text("Binaural beats for different moods").font(.custom("American Typewriter", size: 14.0))
                    Spacer()
                }
            }.padding()
            ModalPresenter {
                if #available(iOS 14.0, *) {
                    ScrollView {
                        LazyVStack {
                            ForEach(Types.allCases) { type in
                                ModalLink(destination: MeditationSwiftUI(type: type.rawValue)) {
                                    TypeCell(type: type.rawValue)
                                }
                            }
                        }.padding(15.0)
                    }
                } else {
                    // Fallback on earlier versions
                    List(Types.allCases) { type in
                        ModalLink(destination: MeditationSwiftUI(type: type.rawValue)) {
                            TypeCell(type: type.rawValue)
                        }
                    }
                }
                
            }
        }
    }
}

struct TypesListSwiftUIView_Previews: PreviewProvider {
    static var previews: some View {
        TypesListSwiftUIView()
    }
}


