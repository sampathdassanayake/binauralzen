//  Copyright © 2020 Sampath Dassanayake. All rights reserved.

import SwiftUI

struct ProgressCircle: View {
    @Binding var remaining: Float
    @Binding var duration: Float
    
    var body: some View {
        ZStack {
            Circle()
                .stroke(lineWidth: 15.0)
                .opacity(0.3)
                .foregroundColor(Color("Home"))
            
            Circle()
                .trim(from: 0.0, to: CGFloat((duration - remaining) / duration))
                .stroke(style: StrokeStyle(lineWidth: 15.0, lineCap: .round, lineJoin: .round))
                .foregroundColor(Color("Home"))
                .rotationEffect(Angle(degrees: 270.0))
                .animation(.linear)

            Text(Helpers.secondsToTimeDisplayable(seconds: Int(self.remaining)))
                .font(.custom("American Typewriter", size: 30.0))
            
        }
    }
}
