//  Copyright © 2020 Sampath Dassanayake. All rights reserved.

import SwiftUI

struct InfoView: View {

    // Variable to Dismiss Modal
//    @Environment(\.presentationMode) var presentationMode: Binding<PresentationMode>

    var body: some View {
        ScrollView {
            VStack {
                Text("What is it?").font(.custom("American Typewriter", size: 18.0)).bold().padding()
                Text("""
                Binaural Beats are when two different tones usually less than a 40 Hz difference difference between them are played, one in each ear, at the same time. An auditory illusion is created and the individual hears a third tone in their mind, with that third tone being the "binaural beat".

                You need stereo headphones for this to work.
                """).lineLimit(nil)
                    .font(.custom("American Typewriter", size: 15.0))
                    .padding()
                Text("Warning").font(.custom("American Typewriter", size: 14.0))
                    .bold()
                    .padding()
                Text("""
                Binaural meditations are safe for most individuals to enjoy. However, there are not recomended for:
                • People who suffer from seizures.
                • People with heart conditions or who have a pacemaker.
                • People who are taking tranquilizers, or who suffer from mental or psychological disorders.
                • Children.
                • Pregnant women.
                • People who are working, driving or operating heavy or large machinery.
                """).lineLimit(nil)
                    .font(.custom("American Typewriter", size: 15.0))
                    .padding()
                Spacer()
                HStack {
                    Text("Made with")
                    Image(systemName: "heart.fill").foregroundColor(.red)
                    Text("in Melbourne")
                    }.font(.custom("American Typewriter", size: 12.0))
    
                
            }
        }
    }
}

struct InfoView_Previews: PreviewProvider {
    static var previews: some View {
        InfoView()
    }
}
