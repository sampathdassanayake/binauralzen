//  Copyright © 2020 Sampath Dassanayake. All rights reserved.
import AudioKit
import HealthKit
import SwiftUI
import Repeat

import MediaPlayer

struct MeditationSwiftUI: View {
    @State var isPlaying: Bool = false
    @State var generator: AKOperationGenerator?
    @State private var freq: Double = 400
    @State private var volume: Double = 25
    @State private var duration: Float = 1800
    @State private var remaining: Float = 1800
    @State private var startTime: Date?
    @State var timer: Repeater?
    var type: Type
    let healthStore = HKHealthStore()
    
    var body: some View {
        ZStack {
            VStack {
                Spacer().frame(minHeight: 10, maxHeight: 20)
                Text(type.displayName)
                    .foregroundColor(Color("Home"))
                    .font(.custom("American Typewriter", size: 48.0)).bold()
                Spacer().frame(minHeight: 10, maxHeight: 50)
                Text(type.description)
                    .foregroundColor(Color("Home"))
                    .font(.custom("American Typewriter", size: 16.0))
                
                Spacer().frame(minHeight: 10, maxHeight: 10)
                ProgressCircle(remaining: $remaining, duration: $duration).padding(40.0)
        
                VStack {
                    HStack {
                        Image(systemName: "clock")
                        Slider(value: $duration, in: 0...3599, step: 1) { _ in
                            self.remaining = self.duration
                        }.accentColor(Color("Home"))
                        Image(systemName: "clock.fill")
                    }.padding(.bottom, -5)
                    Text(Helpers.secondsToTimeDisplayable(seconds: Int($duration.wrappedValue)))
                    .frame(maxWidth: .infinity, alignment: .trailing).font(.custom("American Typewriter", size: 10.0))
                }.foregroundColor(Color("Home"))
                    .padding(.bottom, 1)
                VStack {
                    HStack {
                        Image(systemName: "waveform.path.ecg")
                        Slider(value: $freq, in: 100...1500, step: 1) { _ in
                            self.generator?.parameters[0] = self.$freq.wrappedValue
                        }.accentColor(Color("Home"))
                        Image(systemName: "waveform.path")
                    }.padding(.bottom, -5)
                    Text(String(format: "%.0f Hz", $freq.wrappedValue))
                        .frame(maxWidth: .infinity, alignment: .trailing).font(.custom("American Typewriter", size: 10.0))
                }.foregroundColor(Color("Home"))
                .padding(.bottom, 1)
                
                VStack {
                    HStack {
                        Image(systemName: "speaker")
                        Slider(value: $volume, in: 0...100, step: 1) { _ in
                            self.generator?.parameters[1] = self.$volume.wrappedValue
                        }.accentColor(Color("Home"))
                        Image(systemName: "speaker.3")
                    }.padding(.bottom, -5)
                    Text(String(format: "%.0f %%", $volume.wrappedValue))
                    .frame(maxWidth: .infinity, alignment: .trailing).font(.custom("American Typewriter", size: 10.0))
                }.foregroundColor(Color("Home"))
                    .padding(.bottom, 1)
                
                Button(action: {
                    if self.isPlaying {
                        self.stopAudio()
                    } else {
                        self.playAudio()
                    }
                }) {
                    Image(systemName: isPlaying ? "pause.circle" : "play.circle")
                        .resizable()
                        .frame(width: 60.0, height: 60.0, alignment: .center)
                        .foregroundColor(Color("Home"))
                }
            }
            .padding()
            .navigationBarTitle(type.displayName)
        }.onAppear() {
//            try? AudioKit.start()
            
        }.onDisappear {
            if self.isPlaying {
                self.stopAudio()
                self.saveMindfulMinutes()
            }
            try? AKManager.stop()
            self.generator = nil
        }
    }
    
    func playAudio() {
        guard self.remaining > 0 else { return }
        let completion = {
            if self.generator == nil {
                self.generator = self.createGenerator()
                self.generator?.parameters = [self.$freq.wrappedValue, self.$volume.wrappedValue]
                
                do {
                    try AKSettings.setSession(category: .playback, with: [.allowBluetooth, .allowBluetoothA2DP])
                } catch {
                    print("error")
                }
                AKSettings.playbackWhileMuted = true
                AKManager.output = self.generator
                                    
                try? AKManager.start()
            }
        
            self.generator?.start()
            self.startTimer()
            self.isPlaying = true
        }
        
        Helpers.activateHealthKit { (state, error) in
            completion()
        }
    }
    
    
    func stopAudio() {
        generator?.stop()
        self.isPlaying = false
        self.timer = nil
        saveMindfulMinutes()
    }
    
    
    func createGenerator() -> AKOperationGenerator {
        let temp = AKOperationGenerator(channelCount: 2) { parameters in
            let highFrequency = parameters[0] + (type.frequencyRange.lowerBound + type.frequencyRange.upperBound) / 2
            let leftOutput = AKOperation.sineWave(frequency: parameters[0], amplitude: 1) * parameters[1].portamento(halfDuration: 0.1)
            let rightOutput = AKOperation.sineWave(frequency: highFrequency, amplitude: 1) * parameters[1].portamento(halfDuration: 0.1)
            return [leftOutput, rightOutput]
        }
        return temp
    }
    
    
    func saveMindfulMinutes() {
        if let categoryType = HKObjectType.categoryType(forIdentifier: .mindfulSession),
            let startTime = startTime {
            let sample = HKCategorySample(type: categoryType, value: 0,
                                          start: startTime,
                                          end: Date())
            healthStore.save(sample, withCompletion: { (success, error) -> Void in
                if error != nil {return}
            })
        }
    }
    
    func startTimer() {
        self.timer = Repeater.every(.seconds(1), count: Int(remaining)) { timer in
            guard self.isPlaying else { return }
            if self.remaining > 0 {
                self.remaining -= 1
            } else {
                self.stopAudio()
            }
        }
        self.startTime = Date()
    }
    
}

struct MeditationSwiftUI_Previews: PreviewProvider {
    static var previews: some View {
        MeditationSwiftUI(type: Type.init(stringLiteral: "alpha"))
    }
}

enum HealthkitSetupError: Error {
  case notAvailableOnDevice
  case dataTypeNotAvailable
  case notGranted
}
