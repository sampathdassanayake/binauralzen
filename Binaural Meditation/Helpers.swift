//  Copyright © 2020 Sampath Dassanayake. All rights reserved.

import Foundation
import HealthKit

class Helpers {
    static func secondsToTimeDisplayable(seconds: Int) -> String {
        return String(format: "%02d:%02d", seconds / 60 , seconds % 60)
    }
    
    static func dateFormatter() -> DateFormatter {
        let formatter = DateFormatter()
        formatter.dateFormat = "dd MMM yyyy @ hh:mm aaa"
        return formatter
    }
    
    static func activateHealthKit(completion: @escaping (Bool, Error?) -> Swift.Void) {
        guard HKHealthStore.isHealthDataAvailable() else {
            completion(false, HealthkitSetupError.notAvailableOnDevice)
            return
        }
        let healthStore = HKHealthStore()
        let typestoRead = Set([HKObjectType.categoryType(forIdentifier: HKCategoryTypeIdentifier.mindfulSession)!])
        let typestoShare = Set([HKObjectType.categoryType(forIdentifier: HKCategoryTypeIdentifier.mindfulSession)!])
        
        // Prompt the User for HealthKit Authorization
        healthStore.requestAuthorization(toShare: typestoShare, read:  typestoRead) { (success, error) -> Void in
            if !success{
                completion(false, HealthkitSetupError.notGranted)
            } else {
                completion(true, nil)
            }
            //            self.retrieveMindFulMinutes()
        }
    }
}
